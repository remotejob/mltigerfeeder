package config

import (
	"database/sql"
	"log"

	"github.com/enriquebris/goconcurrentqueue"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/remotejob/mltigerfeeder/pkg/dbhandler"

	"github.com/spf13/viper"
)

type Constants struct {
	PORT   string
	Sqlite struct {
		Url    string
		Urldel string
	}
	RECNUM   string
	BPHRASES int64
	HITS int64
}

type Config struct {
	Constants
	SqliteDb    *sql.DB
	SqliteDbdel *sql.DB
	Queue       *goconcurrentqueue.FIFO
	Bestphrases []string
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}

	db, err := sql.Open("sqlite3", "file:"+config.Sqlite.Url+"?cache=shared&mode=rwc")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	config.SqliteDb = db

	dbdel, err := sql.Open("sqlite3", "file:"+config.Sqlite.Urldel+"?cache=shared&mode=rwc")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	config.SqliteDbdel = dbdel

	config.Queue = goconcurrentqueue.NewFIFO()

	bestphrases, err := dbhandler.GetBestPhrases(config.SqliteDb, int(config.Constants.BPHRASES))

	config.Bestphrases = bestphrases

	return &config, err
}

func initViper() (Constants, error) {
	viper.SetConfigName("mltigerfeeder.conf") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                  // Search the root directory for the configuration file
	err := viper.ReadInConfig()               // Find and read the config file
	if err != nil {                           // Handle errors reading the config file
		return Constants{}, err
	}

	// viper.SetDefault("PORT", "6000")
	// viper.SetDefault("RECNUM", 100)
	viper.SetDefault("HITS",0)
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
