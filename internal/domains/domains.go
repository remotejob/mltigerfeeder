package domains

type Phrase struct {
	Rowid int64

	Title string

	Txt string
}

type InsertData struct {
	Prompt     string
	Bestphrase string
	Orgtxt     string
}
