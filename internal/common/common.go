package common

import (
	"math/rand"
	"time"
)

func RandInt(n int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(n)
}

func Even(number int) bool {
    return number%2 == 0
}
func Odd(number int) bool {
    // Odd should return not even.
    // ... We cannot check for 1 remainder.
    // ... That fails for negative numbers.
    return !Even(number)
}