package randbestphrases

import (
	"math/rand"
	"time"

	"gitlab.com/remotejob/mltigerfeeder/internal/config"
)

func GetBest(conf *config.Config, needednum int) []string {

	var resout []string

	rand.Seed(time.Now().UnixNano())

	for i := 1; i < needednum; i++ {
		rand.Seed(time.Now().UnixNano())
		resout = append(resout, conf.Bestphrases[rand.Intn(int(conf.Constants.BPHRASES))])

	}

	return resout

}
