package capFirstChar

import "unicode"

func Cap(phrase string) string {
	for index, value := range phrase {
		return string(unicode.ToUpper(value)) + phrase[index+1:]
	}
	return ""
}
