package dbhandler

import (
	"database/sql"

	"gitlab.com/remotejob/mltigerfeeder/internal/domains"
)

func InsertQue(db *sql.DB, insertDatas []domains.InsertData) error {

	var stmt *sql.Stmt
	var err error

	for _, insertData := range insertDatas {

		prompt := insertData.Prompt
		bestphrase := insertData.Bestphrase
		orgtxt := insertData.Orgtxt

		sqlStatement0 := `insert into mlphrases(prompt,orgtxt) values(?,?);`
		sqlStatement1 := `insert into mlphrasesbest(txt) values(?);`

		stmt, err = db.Prepare(sqlStatement0)
		if err != nil {
			return err
		}
		_, err = stmt.Exec(prompt, orgtxt)
		if err != nil {

			return err

		}

		if len(bestphrase) > 0 {

			stmt, err = db.Prepare(sqlStatement1)
			if err != nil {
				return err
			}
			_, err = stmt.Exec(bestphrase)
			if err != nil {

				return err

			}

		}

		stmt.Close()
	}
	return nil
}

func Insert(db *sql.DB, insertData domains.InsertData) error {

	var stmt *sql.Stmt
	var err error

	prompt := insertData.Prompt
	bestphrase := insertData.Bestphrase
	orgtxt := insertData.Orgtxt

	sqlStatement0 := `insert into mlphrases(prompt,orgtxt) values(?,?);`
	sqlStatement1 := `insert into mlphrasesbest(txt) values(?);`

	stmt, err = db.Prepare(sqlStatement0)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(prompt, orgtxt)
	if err != nil {

		return err

	}

	if len(bestphrase) > 0 {

		stmt, err = db.Prepare(sqlStatement1)
		if err != nil {
			return err
		}
		_, err = stmt.Exec(bestphrase)
		if err != nil {

			return err

		}

	}

	stmt.Close()
	return nil
}

func CountRec(db *sql.DB) (int64, error) {

	sqlStatement := `SELECT count(*) FROM mlphrases where elab = 0;`

	var rownum int64
	row := db.QueryRow(sqlStatement)
	switch err := row.Scan(&rownum); err {
	case sql.ErrNoRows:
		return 0, err

	case nil:

		return rownum, nil
	default:

		return 0, err

	}

}

func GetBestPhrases(db *sql.DB, quant int) ([]string, error) {

	sqlStatement := "SELECT txt FROM mlphrasesbest ORDER BY random() limit ?"

	var phrs []string
	rows, err := db.Query(sqlStatement, quant)
	if err != nil {

		return nil, err
	}

	for rows.Next() {
		var phrase string
		rows.Scan(&phrase)
		phrs = append(phrs, phrase)

	}

	return phrs, nil

}

func GetDelPhrase(db *sql.DB, dbdel *sql.DB) (domains.Phrase, error) {

	sqlStatement := `SELECT rowid,prompt,orgtxt FROM mlphrases ORDER BY random() limit 1;`

	var prompt string
	var phrase string
	var rowid int64
	var phr domains.Phrase
	row := db.QueryRow(sqlStatement)
	switch err := row.Scan(&rowid, &prompt, &phrase); err {
	case sql.ErrNoRows:
		return phr, err

	case nil:
		// log.Println("rowid", rowid)

		err := DeletePhrase(dbdel, rowid)
		if err != nil {
			return phr, err
		}

		phr = domains.Phrase{rowid, prompt, phrase}
		return phr, nil
	default:

		return phr, err

	}

}

func GetDelPhrases(db *sql.DB, dbdel *sql.DB, rec string) ([]domains.Phrase, error) {

	sqlStatement := "SELECT rowid,prompt,orgtxt FROM mlphrases ORDER BY random() limit " + rec

	var retres []domains.Phrase
	var prompt string
	var phrase string
	var rowid int64

	rows, err := db.Query(sqlStatement)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		err := rows.Scan(&rowid, &prompt, &phrase)
		if err != nil {
			return nil, err
		}
		phr := domains.Phrase{rowid, prompt, phrase}
		retres = append(retres, phr)

	}

	return retres, nil
}

func DeletePhrase(db *sql.DB, rowid int64) error {

	sqlStatement := `insert into mlphrasestodel values(?);`

	stmt, err := db.Prepare(sqlStatement)
	if err != nil {
		return err
	}
	res, err := stmt.Exec(rowid)
	if err != nil {

		return err
	} else {

		_, err := res.RowsAffected()
		if err != nil {

			return err

		}
	}
	stmt.Close()

	return nil

}
