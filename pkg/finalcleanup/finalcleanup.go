package finalcleanup

import "strings"

func Finalclean(phrase string) string {

	var out string
	out = strings.Replace(phrase, "  ", " ", -1)
	out = strings.Replace(phrase, ". .", ".", -1)
	out = strings.Replace(phrase, " .", ".", -1)
	out = strings.Replace(phrase, "..", ".", -1)
	out = strings.Replace(phrase, " .", ".", -1)
	out = strings.Replace(phrase, "..", ".", -1)
	out = strings.Replace(phrase, " .", ".", -1)

	return out

}
