package cleanphrase

import "strings"

import "gitlab.com/remotejob/mltigerfeeder/pkg/checklastchar"

import "gitlab.com/remotejob/mltigerfeeder/pkg/capFirstChar"

func Clean(phrase string,tochk map[string]struct{}) []string {


	var res []string
	dlines := strings.Split(phrase, "<|endoftext|>")
	dlines = dlines[:len(dlines)-1]

	for _, ln := range dlines {

		clstr := strings.Replace(ln, "<|startoftext|>", "", -1)
		clstr = strings.Replace(clstr, "\n", "", -1)
		clstr = checklastchar.CheckChange(clstr,tochk,". ",false)
		clstr = capFirstChar.Cap(clstr)
		res = append(res, clstr)

	}

	return res

}
