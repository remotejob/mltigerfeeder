package insertnewrecords

import (
	"database/sql"
	"log"

	"gitlab.com/remotejob/mltigerfeeder/internal/domains"
	"gitlab.com/remotejob/mltigerfeeder/pkg/dbhandler"
)

func Insert(allquerec []domains.InsertData) {

	db, err := sql.Open("sqlite3", "file:mlphrasesfinew.db?cache=shared&mode=rwc")
	if err != nil {
		log.Println(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}

	err = dbhandler.InsertQue(db, allquerec)
	if err != nil {
		log.Println("DB error return que back", err)

	}
	db.Close()

}
