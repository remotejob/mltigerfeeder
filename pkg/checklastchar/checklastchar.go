package checklastchar

import (
	"strings"
	"unicode/utf8"

	"gitlab.com/remotejob/mltigerfeeder/internal/common"
	"gitlab.com/remotejob/mltigerfeeder/pkg/capFirstChar"
)

func CheckChange(phrase string, tochk map[string]struct{}, subto string, css bool) string {

	var res string

	sphrase := strings.TrimSpace(phrase)

	r, size := utf8.DecodeLastRuneInString(sphrase)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}

	lastchar := phrase[len(sphrase)-size:]

	if _, OK := tochk[lastchar]; OK {

		res = sphrase + " "

	} else {

		res = sphrase + ". "
	}
	res = capFirstChar.Cap(res)
	if css {
		varcss := common.RandInt(4)
		if varcss == 0 {
			res = "<h3>" + res + "</h3>"
		} else if varcss == 1 {
			res = "<h4>" + res + "</h4>"
		} else if varcss == 2 {
			res = "<h5>" + res + "</h5>"
		} else {
			res = "<p><strong>" + res + "</strong></p>"
		}

	}

	return res
}
