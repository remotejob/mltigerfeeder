package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"gitlab.com/remotejob/mltigerfeeder/internal/config"
	"gitlab.com/remotejob/mltigerfeeder/pkg/handler"
	"gitlab.com/remotejob/mltigerfeeder/pkg/handler1"
)

func Routes(configuration *config.Config) *chi.Mux {
	router := chi.NewRouter()

	router.Use(

		middleware.Logger,          // Log API request calls
		// middleware.DefaultCompress, // Compress results, mostly gzipping assets and json
		middleware.RedirectSlashes, // Redirect slashes to no slash URL versions
		middleware.Recoverer,       // Recover from panics without crashing server

	)

	router.Route("/v0", func(r chi.Router) {
		r.Mount("/", handler.New(configuration).Routes())
	})
	router.Route("/v1", func(r chi.Router) {
		r.Mount("/", handler1.New(configuration).Routes())
	})

	return router
}

func main() {

	configuration, err := config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}

	router := Routes(configuration)

	log.Println("Serving application at PORT :" + configuration.Constants.PORT)
	log.Fatal(http.ListenAndServe(":"+configuration.Constants.PORT, router))
}
